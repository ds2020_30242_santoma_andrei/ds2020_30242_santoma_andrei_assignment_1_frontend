import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const ProtectedMedicationContainer = ({ component: Component, role, ...rest }) => {
    return (
        <Route {...rest} render={
            props => {
                if (role === 'doctor') {
                    return <Component {...rest} {...props} />
                } else {
                    return <Redirect to={
                        {
                            pathname: '/forbidden',
                            state: {
                                from: props.location
                            }
                        }
                    } />
                }
            }
        } />
    )
}

export default ProtectedMedicationContainer;