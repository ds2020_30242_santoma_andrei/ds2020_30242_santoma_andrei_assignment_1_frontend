import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    getAllMedications: '/medications/getall',
    medications: '/medications'
};

function getAllMedications(callback) {
    let request = new Request(HOST.backend_api + endpoint.getAllMedications , {
        method: 'GET',
        headers : {
            'Accept': 'application/json',
        }
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function postMedication(medication, callback) {
    let request = new Request(HOST.backend_api + endpoint.medications, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    })

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function putMedication(medication, callback) {
    let request = new Request(HOST.backend_api + endpoint.medications, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    })

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedication(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.medications + '?id=' + id, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
        },
    })

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getAllMedications,
    postMedication,
    putMedication,
    deleteMedication
}