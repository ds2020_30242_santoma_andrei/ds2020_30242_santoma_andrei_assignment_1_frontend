import React from "react";
import Table from "../../commons/tables/table";
import {Button} from "reactstrap";

class DoctorMedicationTable extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tableData: this.props.tableData
        };
    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={[
                    {
                      Header: 'Id',
                      accessor: 'medication_id',
                      show: false,
                    },
                    {
                        Header: 'Name',
                        accessor: 'name',
                    },
                    {
                        Header: 'Side effects',
                        accessor: 'sideEffects',
                    },
                    {
                        Header: 'Dosage',
                        accessor: 'dosage',
                    },
                    {
                        Header: '',
                        Cell: cell => (<Button color='primary'
                                               onClick={() => this.props.updateButtonClick(cell.original.medication_id)}>
                            Update
                        </Button>)
                    },
                    {
                        Header: '',
                        Cell: cell => (<Button color='primary'
                                               onClick={() => this.props.deleteButtonClick(cell.original.medication_id)}>
                            Delete
                        </Button>)
                    }
                ]}
                search={[]}
                pageSize={5}
            />
        )
    }
}

export default DoctorMedicationTable