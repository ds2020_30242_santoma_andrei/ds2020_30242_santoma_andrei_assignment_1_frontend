import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import DoctorMedicationTable from "./components/doctor-medication-table";
import * as API_MEDICATION from "./api/doctor-medication-api"

import Cookies from 'js-cookie';
import CaregiverTable from "../caregiver/components/caregiver-table";
import CaregiverInsertForm from "../caregiver/components/caregiver-insert-form";
import CaregiverUpdateForm from "../caregiver/components/caregiver-update-form";
import MedicationForm from "./components/medication-form";

class MedicationContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            selectedInsert: false,
            selectedUpdate: false,
            medicationUpdateId: '',

        };

        this.toggleInsertForm = this.toggleInsertForm.bind(this);
        this.reloadInsert = this.reloadInsert.bind(this);
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);
        this.clickUpdateButton = this.clickUpdateButton.bind(this);
        this.deleteMedication = this.deleteMedication.bind(this);
    }

    componentDidMount() {
        this.fetchMedications();
    }

    fetchMedications() {
        return API_MEDICATION.getAllMedications(
            (result, status, err) => {
                if (result !== null && status === 200) {
                    this.setState({
                        tableData: result,
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }))
                }
            }
        )
    }


    toggleInsertForm() {
        this.setState({selectedInsert: !this.state.selectedInsert});
    }

    toggleUpdateForm() {
        this.setState({selectedUpdate: !this.state.selectedUpdate});
    }

    deleteMedication(id) {
        return API_MEDICATION.deleteMedication(id, (result, status, err) => {
            if (result === true && status === 200) {
                this.setState({
                    isLoaded: false
                });
                this.fetchMedications()
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }))
            }
        })
    }


    reloadInsert() {
        this.setState({
            isLoaded: false
        });
        this.toggleInsertForm();
        this.fetchMedications();
    }

    reloadUpdate() {
        this.setState({
            isLoaded: false
        });
        this.toggleUpdateForm();
        this.fetchMedications();
    }

    clickUpdateButton(id) {
        this.setState({medicationUpdateId: id})
        this.toggleUpdateForm()
    }


    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Logged in as {Cookies.get('username')}</strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleInsertForm}>Add Medication </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <DoctorMedicationTable tableData={this.state.tableData} deleteButtonClick={this.deleteMedication} updateButtonClick={this.clickUpdateButton}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selectedInsert} toggle={this.toggleInsertForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleInsertForm}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationForm reloadHandler={this.reloadInsert} action='insert'/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}> Update Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationForm reloadHandler={this.reloadUpdate} action='update' medicationUpdateId={this.state.medicationUpdateId}/>
                    </ModalBody>
                </Modal>

            </div>
        )
    }

}

export default MedicationContainer;