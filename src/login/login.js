import React from 'react';

import {
    Button,
    Col,
    Label,
    Input,
    Row,
    Modal,
    ModalBody,
    ModalHeader
} from 'reactstrap';

import Cookies from 'js-cookie';

import * as API_LOGIN from "./api/login-api"
import {Redirect} from "react-router-dom";


class LoginContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username : '',
            password : '',
            buttonDisabled : false,
            showFailedLoginPopup : false,
        };

        this.setInputValue = this.setInputValue.bind(this);
        this.login = this.login.bind(this);
        this.toggleShowFailedLoginPopup = this.toggleShowFailedLoginPopup.bind(this)
    }

    componentDidMount() {

    }

    async login() {
        this.setState({buttonDisabled : true});
        let user = {
            username : this.state.username,
            password : this.state.password
        }
        return API_LOGIN.loginUser(user, (result, status, err) => {
            console.log(result)
            if (result.username !== '' && result.role !== '' && status === 200) {
                console.log("Logged in as " + result.username + ' (' + result.role + ')');
                Cookies.set('username', result.username);
                Cookies.set('role', result.role);
            }
            else {
                this.setState({showFailedLoginPopup : true});
            }

            this.setState({buttonDisabled : false})

        });
    }

    setInputValue(state, value) {
        this.setState({
            [state] : value,
        })
    }

    toggleShowFailedLoginPopup() {
        this.setState({showFailedLoginPopup: !this.state.showFailedLoginPopup})
    }

    render() {

        if (Cookies.get('username')) {
            return (<Redirect to = "/"/>)
        }

        else return (
            <div className="fullscreen">

                <Modal isOpen={this.state.showFailedLoginPopup} toggle={this.toggleShowFailedLoginPopup}>
                    <ModalBody>Wrong username or password!</ModalBody>
                </Modal>

                <div>
                    <Row>
                        <Col sm={{size: '1', offset: 0}}>
                            <Label for='usernameField'> Username: </Label>
                        </Col>
                        <Col sm={{size: '6', offset: 2}}>
                            <Input placeholder='Type here...'
                                onChange={(val) => this.setInputValue('username', val.target.value)}
                            />
                        </Col>
                    </Row>
                </div>
                <div className='paddingTop'>
                    <Row>
                        <Col sm={{size: '1', offset: 0}}>
                            <Label for='passwordField'> Password: </Label>
                        </Col>
                        <Col sm={{size: '6', offset: 2}}>
                            <Input placeholder='Type here...'
                                onChange={(val) => this.setInputValue('password', val.target.value)}
                            />
                        </Col>
                    </Row>
                </div>
                <div className='paddingTop'>
                    <Button color='primary' onClick={this.login} disabled={this.state.buttonDisabled}>
                        Login
                    </Button>
                </div>

            </div>
        )

    }
}


export default LoginContainer;
