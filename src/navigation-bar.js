import React from 'react'
import logo from './commons/images/icon.png';


import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';
import {Button} from "react-bootstrap";

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const NavigationBar = (props) => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>

                {props.userRole === 'doctor' &&
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                        Menu
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/caregivers">Caregivers</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink href="/patients">Patients</NavLink>
                        </DropdownItem>

                        <DropdownItem>
                            <NavLink href="/medications">Medications</NavLink>
                        </DropdownItem>

                    </DropdownMenu>
                </UncontrolledDropdown>}
                {props.userRole === 'caregiver' &&
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                        Menu
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/mypatients">My patients</NavLink>
                        </DropdownItem>

                    </DropdownMenu>
                </UncontrolledDropdown>}
                {props.userRole === 'patient' &&
                <UncontrolledDropdown nav inNavbar>
                    <DropdownToggle style={textStyle} nav caret>
                       Menu
                    </DropdownToggle>
                    <DropdownMenu right >

                        <DropdownItem>
                            <NavLink href="/mymedicationplans">My plans</NavLink>
                        </DropdownItem>


                    </DropdownMenu>
                </UncontrolledDropdown>}

            </Nav>
            <Button onClick={props.onClickLoginButton}>
                {props.buttonText}
            </Button>
        </Navbar>
    </div>
);

export default NavigationBar
