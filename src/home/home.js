import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Container, Jumbotron} from 'reactstrap';
import NavigationBar from "../navigation-bar";

import Cookies from 'js-cookie';
import {Redirect} from "react-router-dom";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class Home extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            buttonText : 'Login',
            redirectToLogin : false,
        };

        this.loginButtonPress = this.loginButtonPress.bind(this);
    }

    componentDidMount() {
        if (Cookies.get('username')) {
            this.setState({buttonText : 'Logout'})
        }
        else {
            this.setState({buttonText : 'Login'})
        }
    }

    loginButtonPress() {
        if (Cookies.get('username')) {
            Cookies.remove('username')
            Cookies.remove('role')
            this.setState({buttonText : 'Login'})
        }
        else {
            this.setState({redirectToLogin : true, buttonText : 'Logout'})
        }
    }

    render() {
        if (this.state.redirectToLogin) {
            return <Redirect to = "/login" />
        }
        else return (

            <div>
                <NavigationBar buttonText={this.state.buttonText} onClickLoginButton={this.loginButtonPress} userRole={Cookies.get('role')}/>
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>Integrated Medical Monitoring Platform for Home-care assistance</h1>
                        <p className="lead" style={textStyle}> <b>Enabling real time monitoring of patients, remote-assisted care services and
                            smart intake mechanism for prescribed medication.</b> </p>
                        <hr className="my-2"/>
                        <p  style={textStyle}> <b>This assignment represents the first module of the distributed software system "Integrated
                            Medical Monitoring Platform for Home-care assistance that represents the final project
                            for the Distributed Systems course. </b> </p>
                        <p className="lead">
                            <Button color="primary" onClick={() => window.open('http://coned.utcluj.ro/~salomie/DS_Lic/')}>Learn
                                </Button>
                        </p>
                    </Container>
                </Jumbotron>

            </div>
        )
    };
}

// If we don't do this the class won't be visible from the outside
export default Home
