import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';
import PersonForm from "./components/person-form";
import PersonDeleteForm from "./components/person-delete-form";

import * as API_USERS from "./api/person-api"
import PersonTable from "./components/person-table";



class PersonContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleInsertForm = this.toggleInsertForm.bind(this);
        this.toggleDeleteForm = this.toggleDeleteForm.bind(this);
        this.reloadInsert = this.reloadInsert.bind(this);
        this.reloadDelete = this.reloadDelete.bind(this);
        this.state = {
            selectedInsert: false,
            selectedDelete: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchPersons();
    }

    fetchPersons() {
        return API_USERS.getPersons((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleInsertForm() {
        this.setState({selectedInsert: !this.state.selectedInsert});
    }

    toggleDeleteForm() {
        this.setState({selectedDelete: !this.state.selectedDelete});
    }


    reloadInsert() {
        this.setState({
            isLoaded: false
        });
        this.toggleInsertForm();
        this.fetchPersons();
    }

    reloadDelete(){
        this.setState({
            isLoaded: false
        });
        this.toggleDeleteForm();
        this.fetchPersons();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Person Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleInsertForm}>Add Person </Button>
                        </Col>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color={"primary"} onClick={this.toggleDeleteForm}>Delete Person</Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PersonTable tableData = {this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                                            errorStatus={this.state.errorStatus}
                                                            error={this.state.error}
                                                        />   }
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selectedInsert} toggle={this.toggleInsertForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleInsertForm}> Add Person: </ModalHeader>
                    <ModalBody>
                        <PersonForm reloadHandler={this.reloadInsert}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedDelete} toggle={this.toggleDeleteForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleDeleteForm}> Delete Person: </ModalHeader>
                    <ModalBody>
                        <PersonDeleteForm reloadHandler={this.reloadDelete}/>
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default PersonContainer;
