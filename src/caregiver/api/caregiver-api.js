import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    getAllCaregivers: '/caregivers/getall',
    caregivers: '/caregivers'
};

function getAllCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.getAllCaregivers , {
        method: 'GET',
        headers : {
            'Accept': 'application/json',
        }
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


function postCaregiver(caregiver, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregivers, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    })

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function putCaregiver(caregiver, callback) {

    let request = new Request(HOST.backend_api + endpoint.caregivers, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    })

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteCaregiver(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.caregivers + '?id=' + id, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
        },
    })

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getAllCaregivers,
    postCaregiver,
    deleteCaregiver,
    putCaregiver
};