import React from "react";
import Table from "../../commons/tables/table";
import {Button} from "reactstrap";

class CaregiverTable extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={[
                    {
                        Header: 'Id',
                        accessor: 'caregiver_id',
                        show:false,
                    },
                    {
                        Header: 'Name',
                        accessor: 'name',
                    },
                    {
                        Header: 'Username',
                        accessor: 'username',
                    },
                    {
                        Header: 'Password',
                        accessor: 'password',
                    },
                    {
                        Header: 'Birthdate',
                        accessor: 'birthdate',
                    },
                    {
                        Header: 'Gender',
                        accessor: 'gender',
                    },
                    {
                        Header: 'Address',
                        accessor: 'address'
                    },
                    {
                        Header: '',
                        Cell: cell => (<Button color='primary'
                        onClick={() => this.props.updateButtonClick(cell.original.caregiver_id)}>
                            Update
                        </Button>)
                    },
                    {
                        Header: '',
                        Cell: cell => (<Button color='primary'
                       onClick={() => this.props.deleteButtonClick(cell.original.caregiver_id)}                        >
                            Delete
                        </Button>)
                    }
                ]}
                search={[]}
                pageSize={5}
            />
        )
    }
}

export default CaregiverTable