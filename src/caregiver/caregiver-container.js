import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';


import CaregiverTable from "./components/caregiver-table";
import * as API_CAREGIVER from "./api/caregiver-api"

import Cookies from 'js-cookie';
import CaregiverInsertForm from "./components/caregiver-insert-form";
import CaregiverUpdateForm from "./components/caregiver-update-form";


class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            selectedInsert: false,
            selectedUpdate: false,
            caregiverUpdateId: '',

        };

        this.toggleInsertForm = this.toggleInsertForm.bind(this);
        this.reloadInsert = this.reloadInsert.bind(this);
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);
        this.deleteCaregiver = this.deleteCaregiver.bind(this);
        this.clickUpdateButton = this.clickUpdateButton.bind(this);
    }

    componentDidMount() {
        this.fetchCaregivers();
    }

    toggleInsertForm() {
        this.setState({selectedInsert: !this.state.selectedInsert});
    }

    toggleUpdateForm() {
        this.setState({selectedUpdate: !this.state.selectedUpdate});
    }

    reloadInsert() {
        this.setState({
            isLoaded: false
        });
        this.toggleInsertForm();
        this.fetchCaregivers();
    }

    reloadUpdate() {
        this.setState({
            isLoaded: false
        });
        this.toggleUpdateForm();
        this.fetchCaregivers();
    }

    deleteCaregiver(id) {
        return API_CAREGIVER.deleteCaregiver(id, (result, status, err) => {
            if (result === true && status === 200) {
                this.setState({
                    isLoaded: false
                });
                this.fetchCaregivers()
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }))
            }
        })
    }

    clickUpdateButton(id) {
        this.setState({caregiverUpdateId: id})
        this.toggleUpdateForm()
    }


    fetchCaregivers() {
        return API_CAREGIVER.getAllCaregivers(
            (result, status, err) => {
                if (result !== null && status === 200) {
                    this.setState({
                        tableData: result,
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }))
                }
            }
        )
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Logged in as {Cookies.get('username')}</strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleInsertForm}>Add Caregiver </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <CaregiverTable tableData={this.state.tableData} deleteButtonClick={this.deleteCaregiver} updateButtonClick={this.clickUpdateButton}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selectedInsert} toggle={this.toggleInsertForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleInsertForm}> Add Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverInsertForm reloadHandler={this.reloadInsert}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}> Update Caregiver: </ModalHeader>
                    <ModalBody>
                        <CaregiverUpdateForm reloadHandler={this.reloadUpdate} caregiverId={this.state.caregiverUpdateId}/>
                    </ModalBody>
                </Modal>

                <Modal>

                </Modal>

            </div>
        )
    }

}

export default CaregiverContainer;