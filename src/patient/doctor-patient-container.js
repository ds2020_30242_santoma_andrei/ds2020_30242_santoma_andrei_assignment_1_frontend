import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import PatientTable from "./components/patient-table";
import * as API_CAREGIVER from "../caregiver/api/caregiver-api"
import * as API_PATIENT from "./api/patient-api"

import Cookies from 'js-cookie';
import CaregiverTable from "../caregiver/components/caregiver-table";
import CaregiverInsertForm from "../caregiver/components/caregiver-insert-form";
import PatientInsertForm from "./components/patient-insert-form";
import CaregiverUpdateForm from "../caregiver/components/caregiver-update-form";
import PatientUpdateForm from "./components/patient-update-form";
import MedicationPlanInsertForm from "./components/medication-plan-insert-form";

class DoctorPatientContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            selectedInsert: false,
            selectedUpdate: false,
            selectedAddMP: false,
            patientUpdateId: '',
            patientAddMPId: '',
        };

        this.toggleInsertForm = this.toggleInsertForm.bind(this);
        this.toggleAddMPForm = this.toggleAddMPForm.bind(this);
        this.reloadInsert = this.reloadInsert.bind(this);
        this.toggleUpdateForm = this.toggleUpdateForm.bind(this);
        this.reloadUpdate = this.reloadUpdate.bind(this);
        this.deletePatient = this.deletePatient.bind(this);
        this.clickUpdateButton = this.clickUpdateButton.bind(this);
        this.clickAddMPButton = this.clickAddMPButton.bind(this);
    }

    componentDidMount() {
        this.fetchPatients()
    }

    toggleInsertForm() {
        this.setState({selectedInsert: !this.state.selectedInsert});
    }

    toggleUpdateForm() {
        this.setState({selectedUpdate: !this.state.selectedUpdate});
    }

    toggleAddMPForm() {
        this.setState({selectedAddMP: !this.state.selectedAddMP});
    }


    reloadInsert() {
        this.setState({
            isLoaded: false
        });
        this.toggleInsertForm();
        this.fetchPatients();
    }

    reloadUpdate() {
        this.setState({
            isLoaded: false
        });
        this.toggleUpdateForm();
        this.fetchPatients();
    }

    clickUpdateButton(id) {
        this.setState( {patientUpdateId: id})
        this.toggleUpdateForm()
    }

    clickAddMPButton(id) {
        this.setState({patientAddMPId: id})
        this.toggleAddMPForm()
    }

    deletePatient(id) {
        return API_PATIENT.deletePatient(id, (result, status, err) => {
            if (result === true && status === 200) {
                this.setState({
                    isLoaded: false
                });
                this.fetchPatients()
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }))
            }
        })
    }

    fetchPatients() {
        return API_PATIENT.getAllPatients(
            (result, status, err) => {
                if (result !== null && status === 200) {
                    this.setState({
                        tableData: result,
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }))
                }
            }
        )
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Logged in as {Cookies.get('username')}</strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleInsertForm}>Add Patient </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientTable tableData={this.state.tableData} deleteButtonClick={this.deletePatient} updateButtonClick={this.clickUpdateButton} addMPButtonClick={this.clickAddMPButton}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selectedInsert} toggle={this.toggleInsertForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleInsertForm}> Add Patient: </ModalHeader>
                    <ModalBody>
                        <PatientInsertForm reloadHandler={this.reloadInsert}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedUpdate} toggle={this.toggleUpdateForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleUpdateForm}> Update Patient: </ModalHeader>
                    <ModalBody>
                        <PatientUpdateForm reloadHandler={this.reloadUpdate} patientId={this.state.patientUpdateId}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.selectedAddMP} toggle={this.toggleAddMPForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddMPForm}> Create medication plan: </ModalHeader>
                    <ModalBody>
                        <MedicationPlanInsertForm reloadHandler={this.re} patientId={this.state.patientAddMPId}/>
                    </ModalBody>
                </Modal>

            </div>
        )
    }

}

export default DoctorPatientContainer;

