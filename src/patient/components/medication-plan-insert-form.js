import validate from "./validators/patient-validators";



import {Col, FormGroup, Input, Label, Row} from "reactstrap";
import Button from "react-bootstrap/Button";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";
import React from "react";

import * as API_MEDICATION_PLAN from "../api/medication-plan-api"

class MedicationPlanInsertForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {
            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                intakeIntervals: {
                    value: '',
                    placeholder: 'Intake intervals...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                periodOfTreatment: {
                    value: '',
                    placeholder: 'Period of treatment...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                },
                medicationIds: {
                    value: '',
                    placeholder: 'Medication IDs...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 3,
                        isRequired: true
                    }
                }
            }

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    toggleForm() {
        this.setState( {collapseForm: !this.state.collapseForm});
    }

    handleChange = event => {
        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    }

    registerMedicationPlan(medicationplan) {
        return API_MEDICATION_PLAN.postMedicationPlan(medicationplan, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medication plan with id: " + result);
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        })
    }

    handleSubmit() {
        let medicationplan = {
            patient_id: this.props.patientId,
            treatmentPeriod: this.state.formControls.periodOfTreatment.value,
            intakeIntervals: this.state.formControls.intakeIntervals.value,
            medicationIdList: this.state.formControls.medicationIds.value.split(', ')
        };
        console.log(JSON.stringify(medicationplan));
        this.registerMedicationPlan(medicationplan);
    }

    render() {
        return (
            <div>

                <FormGroup id='intakeIntervals'>
                    <Label for='intakeIntervalsField'> Intake intervals: </Label>
                    <Input name='intakeIntervals' id='intakeIntervalsField' placeholder={this.state.formControls.intakeIntervals.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.intakeIntervals.value}
                           touched={this.state.formControls.intakeIntervals.touched? 1 : 0}
                           valid={this.state.formControls.intakeIntervals.valid}
                           required
                    />
                    {this.state.formControls.intakeIntervals.touched && !this.state.formControls.intakeIntervals.valid &&
                    <div className={"error-message"}> * intakeIntervals must have a valid format</div>}
                </FormGroup>

                <FormGroup id='periodOfTreatment'>
                    <Label for='periodOfTreatmentField'> Period of treatment: </Label>
                    <Input name='periodOfTreatment' id='periodOfTreatmentField' placeholder={this.state.formControls.periodOfTreatment.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.periodOfTreatment.value}
                           touched={this.state.formControls.periodOfTreatment.touched? 1 : 0}
                           valid={this.state.formControls.periodOfTreatment.valid}
                           required
                    />
                    {this.state.formControls.periodOfTreatment.touched && !this.state.formControls.periodOfTreatment.valid &&
                    <div className={"error-message"}> * periodOfTreatment must have a valid format</div>}
                </FormGroup>

                <FormGroup id='medicationIds'>
                    <Label for='medicationIdsField'> Medication IDs: </Label>
                    <Input name='medicationIds' id='medicationIdsField' placeholder={this.state.formControls.medicationIds.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medicationIds.value}
                           touched={this.state.formControls.medicationIds.touched? 1 : 0}
                           valid={this.state.formControls.medicationIds.valid}
                           required
                    />
                    {this.state.formControls.medicationIds.touched && !this.state.formControls.medicationIds.valid &&
                    <div className={"error-message"}> * medicationIds must have a valid format</div>}
                </FormGroup>
                
                
                

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Submit </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
        ) ;
    }

}

export default MedicationPlanInsertForm