import React from "react";
import Table from "../../commons/tables/table";

class MedicationTable extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tableData: this.props.tableData
        };
    }


    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={[
                    {
                        Header: 'Name',
                        accessor: 'name',
                    },
                    {
                        Header: 'Side effects',
                        accessor: 'sideEffects',
                    },
                    {
                        Header: 'Dosage',
                        accessor: 'dosage',
                    }
                ]}
                search={[]}
                pageSize={5}
            />
        )
    }
}

export default MedicationTable