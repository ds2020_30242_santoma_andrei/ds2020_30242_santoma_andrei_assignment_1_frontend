import React from "react";
import Table from "../../commons/tables/table";
import {Button} from "reactstrap";


class MedicationPlansTable extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={[
                    {
                        Header: 'Id',
                        accessor: 'medication_plan_id',
                        show: false,
                    },
                    {
                        Header: 'Intake intervals',
                        accessor: 'intakeIntervals',
                    },
                    {
                        Header: 'Treatment period',
                        accessor: 'treatmentPeriod',
                    },
                    {
                        Header: '',
                        Cell: cell => (<Button color='primary' onClick={() => this.props.viewButtonClick(cell.original.medication_plan_id)}>
                            View
                        </Button>)
                    }
                ]}
                search={[]}
                pageSize={5}
            />
        )
    }
}

export default MedicationPlansTable