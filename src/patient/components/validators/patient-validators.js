
const minLengthValidator = (value, minLength) => {
    return value.length >= minLength;
};

const requiredValidator = value => {
    return value.trim() !== '';
};

const uuidValidator = value => {
    const re = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
    return re.test(String(value));
}

const genderValidator = value => {
    return value === 'Male' || value === 'Female'
}

const birthdateValidator = value => {
    const re = /^((19[0-9])|(20[0-1]))[0-9]\/\d\d\/\d\d$/i;
    return re.test(String(value));
}

const validate = (value, rules) => {
    let isValid = true;

    for (let rule in rules) {

        switch (rule) {
            case 'minLength': isValid = isValid && minLengthValidator(value, rules[rule]);
                break;

            case 'isRequired': isValid = isValid && requiredValidator(value);
                break;

            case 'uuidValidator': isValid = isValid && uuidValidator(value);
                break;

            case 'genderValidator': isValid = isValid && genderValidator(value);
                break;

            case 'birthdateValidator': isValid = isValid && birthdateValidator(value);
                break;

            default: isValid = true;
        }

    }

    return isValid;
};

export default validate;
