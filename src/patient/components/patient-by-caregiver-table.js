import React from "react";
import Table from "../../commons/tables/table";
import {Button} from "reactstrap";

class PatientByCaregiverTable extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={[
                    {
                        Header: 'Id',
                        accessor: 'patient_id',
                        show:false,
                    },
                    {
                        Header: 'Name',
                        accessor: 'name',
                    },
                    {
                        Header: 'Username',
                        accessor: 'username',
                    },
                    {
                        Header: 'Password',
                        accessor: 'password',
                    },
                    {
                        Header: 'Birthdate',
                        accessor: 'birthdate',
                    },
                    {
                        Header: 'Gender',
                        accessor: 'gender',
                    },
                    {
                        Header: 'Address',
                        accessor: 'address'
                    },
                    {
                        Header: 'Medical record',
                        accessor: 'medicalRecord'
                    },
                    {
                        Header: '',
                        Cell: cell => (<Button color='primary'
                                               onClick={() => this.props.viewMPButtonClick(cell.original.username)}>
                            View medication plans
                        </Button>)
                    },

                ]}
                search={[]}
                pageSize={5}
            />
        )
    }
}

export default PatientByCaregiverTable