import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import PatientTable from "./components/patient-table";
import * as API_PATIENT from "./api/patient-api"

import Cookies from 'js-cookie';
import PatientByCaregiverTable from "./components/patient-by-caregiver-table";
import PatientContainer from "./patient-container";
import SockJS from "sockjs-client";
import Stomp from "stompjs";

const socket = new SockJS('http://localhost:8080/notification');
const stompClient = Stomp.over(socket);

class CaregiverPatientContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            patientUsername: '',
            showMedicationPlans: false,
            showNotification: false,
            notificationText: ''
        };


        this.clickViewMPButton = this.clickViewMPButton.bind(this)
        this.toggleShowMedicationPlans = this.toggleShowMedicationPlans.bind(this)
        this.toggleShowNotification = this.toggleShowNotification.bind(this)
        this.processMessage = this.processMessage.bind(this);
    }

    toggleShowNotification() {
        this.setState({showNotification: !this.state.showNotification})
    }

    processMessage(message) {
        if (message['activity'] === 'Sleeping') {
            this.setState({
                showNotification: true,
                notificationText: message['patient_name'] + ' has been sleeping for more than 7 hours!'
            })
        }
        else if (message['activity'] === 'Leaving') {
            this.setState({
                showNotification: true,
                notificationText: message['patient_name'] + ' has been outside for more than 5 hours!'
            })
        }
        else if (message['activity'] === 'Toileting') {
            this.setState({
                showNotification: true,
                notificationText: message['patient_name'] + ' has been in the bathroom for more than 30 minutes!'
            })

        }

    }

    componentDidMount() {
        this.fetchPatients()
        stompClient.connect({}, (frame) => {
            console.log('Connected: ' + frame)
            stompClient.subscribe('/topic/activities', (response) => {this.processMessage(JSON.parse(response.body))});
        })
    }

    toggleShowMedicationPlans() {
        this.setState({showMedicationPlans: !this.state.showMedicationPlans})
    }


    clickViewMPButton(username) {
        this.setState({patientUsername: username})
        this.toggleShowMedicationPlans()
    }

    fetchPatients() {
        return API_PATIENT.getAllPatients(
            (result, status, err) => {
                if (result !== null && status === 200) {
                    this.setState({
                        tableData: result,
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }))
                }
            }
        )
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Logged in as {Cookies.get('username')}. These are your patients</strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientByCaregiverTable tableData={this.state.tableData} viewMPButtonClick={this.clickViewMPButton}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.showMedicationPlans} toggle={this.toggleShowMedicationPlans} size="lg">
                    <ModalHeader toggle={this.toggleShowMedicationPlans}> Medications plans for {this.state.patientUsername}</ModalHeader>
                    <ModalBody>
                        <PatientContainer asCaregiver={true} username={this.state.patientUsername}/>
                    </ModalBody>

                </Modal>

                <Modal isOpen={this.state.showNotification} toggle={this.toggleShowNotification} size="lg">
                    <ModalHeader toggle={this.toggleShowNotification}> Anomaly detected</ModalHeader>
                    <ModalBody>
                        {this.state.notificationText}
                    </ModalBody>
                </Modal>
            </div>
        )
    }

}

export default CaregiverPatientContainer;

