import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    medicationPlansByUsername : '/medicationplans/byusername',
    medicationsByMPId: '/medications/bympid',
    getAllPatients: '/patients/getall',
    patients: '/patients'
}

function getAllPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.getAllPatients, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
        }
    })

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function postPatient(patient, callback) {
    let request = new Request(HOST.backend_api + endpoint.patients, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    })

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function putPatient(patient, callback) {
    let request = new Request(HOST.backend_api + endpoint.patients, {
        method: 'PUT',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    })


    console.log(JSON.stringify(patient))
    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePatient(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.patients + '?id=' + id, {
        method: 'DELETE',
        headers : {
            'Accept': 'application/json',
        },
    })

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getMedicationPlansByUsername(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlansByUsername + '?username=' + username, {
        method: 'GET',
        headers : {
            'Accept' : 'application/json',
        },
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getMedicationsByMPId(id, callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationsByMPId + '?id=' + id, {
        method: 'GET',
        headers : {
            'Accept' : 'application/json',
        },
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getMedicationPlansByUsername,
    getMedicationsByMPId,
    getAllPatients,
    postPatient,
    putPatient,
    deletePatient
};

