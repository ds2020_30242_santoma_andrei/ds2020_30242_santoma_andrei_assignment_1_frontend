import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    medicationPlans: '/medicationplans'
}

function postMedicationPlan(medicationplan, callback) {
    let request = new Request(HOST.backend_api + endpoint.medicationPlans, {
        method: 'POST',
        headers : {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicationplan)
    })

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    postMedicationPlan
};