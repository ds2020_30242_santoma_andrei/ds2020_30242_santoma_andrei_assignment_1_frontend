import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button,
    Card,
    CardHeader,
    Col,
    Modal,
    ModalBody,
    ModalHeader,
    Row
} from 'reactstrap';

import MedicationPlansTable from "./components/medication-plans-table";
import * as API_PATIENT from "./api/patient-api"

import Cookies from 'js-cookie';
import MedicationTable from "./components/medication-table";

class PatientContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showMedications: false,
            tableData: [],
            medicationTableData: [],
            isLoadedMedication: false,
            isLoaded: false,
            errorStatus: 0,
            error: null
        };

        this.viewButtonClick = this.viewButtonClick.bind(this);
        this.toggleShowMedications = this.toggleShowMedications.bind(this);
    }


    viewButtonClick(id) {
        return API_PATIENT.getMedicationsByMPId(
            id,
            (result, status, err) => {
                if (result !== null && status === 200) {
                    this.setState({
                        medicationTableData: result,
                        isLoadedMedication: true,
                        showMedications: true,
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }))
                }
            }
        )
    }

    componentDidMount() {
        this.fetchMedicationPlans();
    }

    toggleShowMedications() {
        this.setState({showMedications: !this.state.showMedications})
    }

    fetchMedicationPlans() {
        if (this.props.asCaregiver === false) {

            return API_PATIENT.getMedicationPlansByUsername(
                Cookies.get('username'),
                (result, status, err) => {
                    if (result !== null && status === 200) {
                        this.setState({
                            tableData: result,
                            isLoaded: true
                        });
                    } else {
                        this.setState(({
                            errorStatus: status,
                            error: err
                        }))
                    }
                }
            )
        }
        else {
            return API_PATIENT.getMedicationPlansByUsername(
                this.props.username,
                (result, status, err) => {
                    if (result !== null && status === 200) {
                        this.setState({
                            tableData: result,
                            isLoaded: true
                        });
                    } else {
                        this.setState(({
                            errorStatus: status,
                            error: err
                        }))
                    }
                }
            )

        }
    }

    render() {
        return (
            <div>
                {this.props.asCaregiver === false && <CardHeader>
                    <strong> Hello, {Cookies.get('username')}. Here are your medication plans </strong>
                </CardHeader>
                }

                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <MedicationPlansTable tableData={this.state.tableData} viewButtonClick={this.viewButtonClick} />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.showMedications} toggle={this.toggleShowMedications} size="lg">
                    <ModalHeader toggle={this.toggleShowMedications}> Medications </ModalHeader>
                    <ModalBody>
                        <MedicationTable tableData={this.state.medicationTableData}/>
                    </ModalBody>

                </Modal>

            </div>
        )
    }
}

export default PatientContainer;