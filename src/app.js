import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Cookies from 'js-cookie';
import Home from './home/home';
import PersonContainer from './person/person-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';
import LoginContainer from "./login/login";
import PatientContainer from "./patient/patient-container";
import CaregiverContainer from "./caregiver/caregiver-container";
import MedicationContainer from "./medication/medication-container";
import DoctorPatientContainer from "./patient/doctor-patient-container";
import CaregiverPatientContainer from "./patient/caregiver-patient-container";
import ProtectedMedicationContainer from "./medication/protected-medication-container";



class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
            <Router>
                <div>
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />

                        <Route
                            exact
                            path='/login'
                            render={() => <LoginContainer/>}
                        />

                        <Route
                            exact
                            path='/person'
                            render={() => <PersonContainer/>}
                        />

                        <Route
                            exact
                            path = '/mypatients'
                            render = {() => <CaregiverPatientContainer/>}
                        />

                        <Route
                            exact
                            path = '/mymedicationplans'
                            render = {() => <PatientContainer asCaregiver={false}/>}
                        />

                        <Route
                            exact
                            path = '/caregivers'
                            render = {() => <CaregiverContainer/>}
                        />

                        <Route
                            exact
                            path = '/patients'
                            render = {() => <DoctorPatientContainer/>}
                        />

                        <ProtectedMedicationContainer
                            exact
                            path = '/medications'
                            role = {Cookies.get('role')}
                            component = {MedicationContainer}
                        />
                        <Route
                            exact
                            path = '/medications'
                            render = {() => <MedicationContainer/>}
                        />

                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                        <Route
                            exact
                            path='/forbidden'
                            render={() => <div>Forbidden access</div>}
                        />

                        <Route render={() =><ErrorPage/>} />
                    </Switch>
                </div>
            </Router>
            </div>
        )
    };
}

export default App
